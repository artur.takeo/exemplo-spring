package br.com.itau.marketplace.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.repositories.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	public void inserirProduto(Produto produto) {
		produtoRepository.save(produto);
	}
	
	public Iterable<Produto> obterProdutos(){
		return produtoRepository.findAll();
	}
}
